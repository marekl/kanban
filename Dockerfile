### STAGE 1: Build ###
FROM node:14-alpine AS builder

RUN mkdir /app && chown node: /app
WORKDIR /app

# Build as non-root to make sure prepare scripts for dependencies are run
USER node
COPY package*.json ./
RUN npm ci
USER root

COPY . .
RUN npm run build

### STAGE 2: Production ###
FROM nginx:stable-alpine
COPY --from=builder /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
