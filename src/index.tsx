import React from 'react';
import ReactDOM from 'react-dom';

import 'react-datetime/css/react-datetime.css';
import './styles/style.scss';

import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
