import { List } from "immutable";
import Column from "./Column";

export type Columns = List<Column>