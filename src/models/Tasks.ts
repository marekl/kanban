import { List } from "immutable";
import Task from "./Task";

export type Tasks = List<Task>;